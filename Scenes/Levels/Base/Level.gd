extends Node

signal all_food_collected
signal instantiated_head
signal ran_out_of_time

export (int)var stage_number = 0
export (int)var time_for_level = 200
export (float)var snake_move_delay = 0.25
export (int)var snake_tail_length = 2
export (AudioStreamOGGVorbis)var level_theme = preload \
		("res://Sounds/Never Stop Running (8-Bit).ogg")

onready var Main = preload("res://Scenes/Menu/Main.tscn")
onready var Snake = preload("res://Scenes/Snake/Snake.tscn")
onready var Food = preload("res://Scenes/Food/Food.tscn")
onready var GameOverPopup = preload("res://Scenes/HUD/GameOverPopup.tscn")

onready var food_left_label = $CanvasLayer/HUD/Background/MarginContainer/\
		HBoxContainer/FoodContainer/FoodLeftLabel
onready var time_left_label = $CanvasLayer/HUD/Background/MarginContainer/\
		HBoxContainer/TimeContainer/TimeLeftLabel
onready var stage_number_label = $CanvasLayer/HUD/Background/MarginContainer/\
		HBoxContainer/StageContainer/StageNumberLabel

onready var second_timer = $SecondTimer
onready var music_player = $MusicPlayer

var snake
var head

var food_spawns
var food_spawns_array
var food_spawns_count
var current_food_spawn
var gameover_popup
var time_left

func _ready():
	set_stage_number_label()
	instantiate_player()
	initialize_food_logic()
	initialize_gameover_popup()
	initialize_second_timer()
	initialize_music_player()

func instantiate_player():
	snake = Snake.instance()
	add_child(snake)
	snake.set_move_delay(snake_move_delay)
	snake.set_init_tail_length(snake_tail_length)
	
	head = snake.get_node("Head")
	head.position = $SnakeSpawn.position
	
	setup_connections()
	
	emit_signal("instantiated_head")

func setup_connections():
	self.connect("instantiated_head", snake, "instantiate_tail")
	self.connect("ran_out_of_time", snake, "kill_snake")
	
	head.connect("collected_food", self, "spawn_next_food")
	head.connect("hit_obstacle", self, "stop_level")
	
	snake.connect("destroyed_whole_tail", self, "display_gameover_popup")
	snake.connect("received_input", self, "start_level")
	snake.connect("escaped_level", self, "stop_level")
	snake.get_node("LevelCompletedSound").connect("finished", self,
			"switch_to_preface_after_level_completion")
			
	second_timer.connect("timeout", self, "second_passed")

func initialize_food_logic():
	food_spawns = $FoodSpawnsContainer
	food_spawns_array = food_spawns.get_children()
	food_spawns_count = food_spawns.get_child_count()
	
	if food_spawns_count == 0:
		printerr("No food found!")
	else:
		current_food_spawn = 0
		spawn_next_food()

func initialize_music_player():
	if level_theme == null:
		level_theme = load("res://Sounds/Never Stop Running (8-Bit).ogg")

	music_player.set("stream", level_theme)
	music_player.play()

func initialize_gameover_popup():
	gameover_popup = GameOverPopup.instance()
	add_child(gameover_popup)

func initialize_second_timer():
	second_timer.wait_time = 1
	time_left = time_for_level
	update_time_left_label()

func spawn_next_food():
	update_food_left_label()
	
	if current_food_spawn < food_spawns_count:
		var new_food = Food.instance()
		add_child(new_food)
		new_food.position = food_spawns_array[current_food_spawn].position
		
		current_food_spawn += 1
	else:
		second_timer.stop()
		emit_signal("all_food_collected")
		print("All food collected! Now escape!")

func start_level(key):
	second_timer.start()
	snake.disconnect("received_input", self, "start_level")

func stop_level():
	music_player.stop()
	second_timer.stop()

func switch_to_preface_after_level_completion():
	global.move_to_next_stage = true
	get_tree().change_scene("res://Scenes/Menu/Main.tscn")

func second_passed():
	time_left -= 1
	update_time_left_label()
	
	if time_left == 0:
		stop_level()
		emit_signal("ran_out_of_time")

func update_food_left_label():
	food_left_label.text = str(food_spawns_count - current_food_spawn).pad_zeros(3)

func update_time_left_label():
	time_left_label.text = str(time_left).pad_zeros(3)

func set_stage_number_label():
	stage_number_label.text = str(stage_number).pad_zeros(3)

func display_gameover_popup():
	gameover_popup.popup()