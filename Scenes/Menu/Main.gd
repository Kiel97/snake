extends CanvasLayer

# Enums
enum MenuScreen {SPLASH_SCREEN, MAIN_MENU, CREDITS, PREFACE}
enum Options {DIFFICULTY, STAGE, START, CREDITS, QUIT}
enum Difficulties {EASY, NORMAL}

# Main variables
var current_menu = MenuScreen.SPLASH_SCREEN
var selected_option = Options.DIFFICULTY
var selected_stage = 1
var selected_difficulty = Difficulties.EASY

# Submenus variables
onready var splash_screen = $Background/SplashScreen
onready var main_menu = $Background/MainMenu
onready var credits = $Background/Credits
onready var preface = $Background/Preface

# Main menu variables
onready var difficulty_arrow = $Background/MainMenu/CenterContainer/\
		GridContainer/ArrowDifficulty
onready var stage_arrow = $Background/MainMenu/CenterContainer/GridContainer/\
		ArrowStage
onready var start_arrow = $Background/MainMenu/CenterContainer/GridContainer/\
		ArrowStart
onready var credits_arrow = $Background/MainMenu/CenterContainer/GridContainer/\
		ArrowCredits
onready var quit_arrow = $Background/MainMenu/CenterContainer/GridContainer/\
		ArrowQuit
onready var selected_stage_label = $Background/MainMenu/CenterContainer/\
		GridContainer/SelectedStageLabel
onready var selected_difficulty_label = $Background/MainMenu/CenterContainer/\
		GridContainer/SelectedDifficultyLabel
onready var selection_texture = preload("res://Images/selection_arrow.png")

# Preface variables
onready var current_stage_label = $Background/Preface/CenterContainer/\
		VBoxContainer/CurrentStageLabel
onready var current_difficulty_label = $Background/Preface/CenterContainer/\
		VBoxContainer/CurrentDifficultyLabel

# Sound related variables
onready var sound_source = $SoundSource
onready var selection_sound = preload("res://Sounds/selection.wav")
onready var cancel_sound = preload("res://Sounds/cancel.wav")

# Path related variables
var path_to_levels = "res://Scenes/Levels"
var level_name = get_level_filename(selected_stage)

func _ready():
	get_saved_infos_from_global()
	set_preface_infos()
	display_current_selection()
	display_current_menu()

func _input(event):
	match current_menu:
		MenuScreen.CREDITS:
			if event.is_action_pressed("ui_accept") or \
			event.is_action_pressed("ui_cancel"):
				set_current_menu(MenuScreen.MAIN_MENU)
				play_cancel_sound()
		MenuScreen.SPLASH_SCREEN:
			if event.is_action_pressed("ui_accept"):
				set_current_menu(MenuScreen.MAIN_MENU)
				play_cancel_sound()
		MenuScreen.MAIN_MENU:
			if event.is_action_pressed("ui_up"):
				change_selected_menu_option(-1)
				play_selection_sound()
			elif event.is_action_pressed("ui_down"):
				change_selected_menu_option(+1)
				play_selection_sound()
			elif event.is_action_pressed("ui_accept"):
				match selected_option:
					Options.START:
						set_preface_infos()
						set_current_menu(MenuScreen.PREFACE)
					Options.QUIT:
						quit_application()
					Options.CREDITS:
						set_current_menu(MenuScreen.CREDITS)
			elif event.is_action_pressed("ui_right"):
				if selected_option == Options.STAGE:
					change_selected_stage(+1)
					play_selection_sound()
				elif selected_option == Options.DIFFICULTY:
					change_selected_difficulty(+1)
					play_selection_sound()
			elif event.is_action_pressed("ui_left"):
				if selected_option == Options.STAGE:
					change_selected_stage(-1)
					play_selection_sound()
				elif selected_option == Options.DIFFICULTY:
					change_selected_difficulty(-1)
					play_selection_sound()
		MenuScreen.PREFACE:
			if event.is_action_pressed("ui_accept"):
				global.save_to_global(current_menu, selected_stage, 
						selected_difficulty)
				start_game()
			elif event.is_action_pressed("ui_cancel"):
				set_current_menu(MenuScreen.MAIN_MENU)
				play_cancel_sound()

func get_saved_infos_from_global():
	if global.saved_menu == null or global.saved_stage == null \
			or global.saved_difficulty == null:
		global.save_to_global(current_menu, selected_stage, selected_difficulty)
	else:
		current_menu = global.saved_menu
		selected_difficulty = global.saved_difficulty
		selected_stage = global.saved_stage
		
		if global.move_to_next_stage == true:
			change_selected_stage(+1)
			global.move_to_next_stage = false

func set_current_menu(menu_type):
	current_menu = menu_type
	display_current_menu()

func display_current_menu():
	main_menu.visible = false
	credits.visible = false
	splash_screen.visible = false
	preface.visible = false
	
	match current_menu:
		MenuScreen.SPLASH_SCREEN:
			splash_screen.visible = true
		MenuScreen.MAIN_MENU:
			main_menu.visible = true
		MenuScreen.CREDITS:
			credits.visible = true
		MenuScreen.PREFACE:
			preface.visible = true

func display_current_selection():
	difficulty_arrow.set_texture(null)
	stage_arrow.set_texture(null)
	start_arrow.set_texture(null)
	credits_arrow.set_texture(null)
	quit_arrow.set_texture(null)
	
	match selected_option:
		Options.DIFFICULTY:
			difficulty_arrow.set_texture(selection_texture)
		Options.STAGE:
			stage_arrow.set_texture(selection_texture)
		Options.START:
			start_arrow.set_texture(selection_texture)
		Options.CREDITS:
			credits_arrow.set_texture(selection_texture)
		Options.QUIT:
			quit_arrow.set_texture(selection_texture)
	
	selected_stage_label.text = str(selected_stage).pad_zeros(3)
	selected_difficulty_label.text = get_selected_difficulty()

func set_preface_infos():
	current_stage_label.text = "Stage: " + str(selected_stage).pad_zeros(3)
	current_difficulty_label.text = get_selected_difficulty()

func get_selected_difficulty():
	match selected_difficulty:
		Difficulties.EASY:
			return "Easy"
		Difficulties.NORMAL:
			return "Normal"
			
func change_selected_menu_option(value):
	selected_option = clamp(selected_option + value, 0, len(Options)-1)
	display_current_selection()

func change_selected_difficulty(value):
	selected_difficulty = clamp(selected_difficulty + value, 0,
			len(Difficulties)-1)
	display_current_selection()

func change_selected_stage(value):
	var new_stage = selected_stage + value
	selected_stage = clamp_selected_stage(new_stage)
	display_current_selection()

func clamp_selected_stage(new_stage):
	var path_to_check = path_to_levels.plus_file(get_level_filename(new_stage))
	var does_level_exist = File.new().file_exists(path_to_check)
	
	print(path_to_check, ": ", does_level_exist)
	
	if does_level_exist:
		return new_stage
	else:
		return selected_stage

func get_level_filename(level_number):
	return "Level" + str(level_number) + get_selected_difficulty() + ".tscn"

func start_game():
	var path = path_to_levels.plus_file(get_level_filename(selected_stage))
	get_tree().change_scene(path)

func play_selection_sound():
	sound_source.set_stream(selection_sound)
	sound_source.play()

func play_cancel_sound():
	sound_source.set_stream(cancel_sound)
	sound_source.play()

func quit_application():
	get_tree().quit()