extends Area2D

signal collected_food
signal hit_obstacle
signal escaped_level
signal moved_head(head_pos)

var sprite_size
var current_direction = directions.NONE
var previous_direction = directions.NONE
var can_change_direction = true

onready var sprite = $Sprite
onready var tween = $Tween

func _ready():
	can_change_direction = true
	sprite_size = sprite.texture.get_size() * sprite.transform.get_scale()
	configure_tween()

func change_direction(request):
	if can_change_direction == true:
		if request == "left" and previous_direction != directions.RIGHT:
			current_direction = directions.LEFT
		elif request == "right" and previous_direction != directions.LEFT:
			current_direction = directions.RIGHT
		elif request == "up" and previous_direction != directions.DOWN:
			current_direction = directions.UP
		elif request == "down" and previous_direction != directions.UP:
			current_direction = directions.DOWN

func move(direction):
	translate(direction * sprite_size)

func _on_MoveTimer_timeout():
	var previous_head_position = position
	move(current_direction)
	previous_direction = current_direction
	
	if current_direction != directions.NONE and \
			previous_direction != directions.NONE:
		emit_signal("moved_head", previous_head_position)

func _on_Head_body_entered(body):
	if body.is_in_group("Food"):
		emit_signal("collected_food")
		body.queue_free()
		print("Collected food!")
	elif body.is_in_group("Exit"):
		emit_signal("escaped_level")
	else:
		hit_obstacle()

func hit_obstacle():
	emit_signal("hit_obstacle")
	tween.start()

func configure_tween():
	tween.interpolate_property(self, "modulate", Color(1,1,1,1), Color(1,1,1,0),
			0.2, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	
	tween.connect("tween_completed", self, "destroy_self")

func force_to_go_up():
	current_direction = directions.UP
	can_change_direction = false

func destroy_self(object, node):
	object.queue_free()