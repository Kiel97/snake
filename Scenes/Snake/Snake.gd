extends Node

signal received_input(key)
signal died
signal destroyed_whole_tail
signal escaped_level

var init_tail_length = 2
var move_delay = 0.25

onready var Tail = preload("res://Scenes/Snake/Tail/Tail.tscn")

onready var head = $Head
onready var move_timer = $MoveTimer
onready var tail_container = $TailContainer

onready var eat_sound = $EatSound
onready var hit_sound = $HitSound
onready var level_completed_sound = $LevelCompletedSound

onready var tail_sprite_size = get_tail_sprite_size()
var current_tail_length = 0
var has_eaten_food = false

func _ready():
	setup_connections()

func _input(event):
	if event.is_action_pressed("ui_left"):
		emit_signal("received_input", "left")
	elif event.is_action_pressed("ui_right"):
		emit_signal("received_input", "right")
	elif event.is_action_pressed("ui_up"):
		emit_signal("received_input", "up")
	elif event.is_action_pressed("ui_down"):
		emit_signal("received_input", "down")

func set_move_delay(move_delay):
	move_timer.set_wait_time(move_delay)

func set_init_tail_length(length):
	init_tail_length = length

func setup_connections():
	self.connect("received_input", head, "change_direction")
	
	head.connect("hit_obstacle", self, "die")
	head.connect("escaped_level", self, "escape_level")
	head.connect("moved_head", self, "move_tail")
	head.connect("collected_food", self, "set_has_eaten_food_true")
	head.connect("tree_exited", self, "destroy_whole_tail")

func instantiate_tail():
	for i in range(1, init_tail_length + 1):
		var tail_pos = head.position + (directions.RIGHT * tail_sprite_size * i)
		grow_tail(tail_pos)
	
func get_tail_sprite_size():
	var tail = Tail.instance()
	var sprite = tail.get_node("Sprite")
	var sprite_size = sprite.texture.get_size() * sprite.transform.get_scale()
	
	tail.free()
	
	return sprite_size

func grow_tail(pos):
	var new_tail = Tail.instance()
	tail_container.add_child(new_tail)
	new_tail.position = pos
	current_tail_length += 1

func move_tail(head_pos):
	if has_eaten_food == true:
		grow_tail(head_pos)
		has_eaten_food = false
		
	if current_tail_length > 0:
		var tail_to_move = tail_container.get_children().pop_back()
		tail_to_move.position = head_pos
		tail_container.move_child(tail_to_move, 0)
		
func destroy_whole_tail():
	for tail in tail_container.get_children():
		tail.run_tween()
		yield(tail, "tree_exited")
	
	emit_signal("destroyed_whole_tail")

func kill_snake():
	head.hit_obstacle()

func die():
	move_timer.stop()
	hit_sound.play()
	emit_signal("died")
	print("Game over!")

func escape_level():
	print("Level completed!")
	head.force_to_go_up()

	var i = 0
	while(i <= current_tail_length + 1):
		yield(move_timer, "timeout")
		i += 1

	move_timer.stop()
	level_completed_sound.play()
	emit_signal("escaped_level")

func set_has_eaten_food_true():
	eat_sound.play()
	has_eaten_food = true