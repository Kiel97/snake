extends StaticBody2D

onready var tween = $Tween

func _ready():
	configure_tween()

func configure_tween():
	tween.interpolate_property(self, "modulate", Color(1,1,1,1), Color(1,1,1,0),
			0.2, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	
	tween.connect("tween_completed", self, "destroy_self")

func run_tween():
	tween.start()

func destroy_self(object, node):
	object.queue_free()