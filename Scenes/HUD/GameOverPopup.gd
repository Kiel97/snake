extends Popup

onready var tween = $Tween

func _ready():
	tween.interpolate_property($Frame, "rect_position", Vector2(208,-64),
			Vector2(208,176), 0.75, Tween.TRANS_LINEAR, Tween.EASE_OUT_IN)

func _input(event):
	if (event.is_action_pressed("ui_cancel") or \
			event.is_action_pressed("ui_accept")) and visible:
		get_tree().change_scene("res://Scenes/Menu/Main.tscn")

func _on_GameOverPopup_about_to_show():
	tween.start()
