extends StaticBody2D

onready var gate_texture = preload("res://Images/grey_block_arrow.png")

func switch_to_gate():
	self.add_to_group("Exit")
	$Sprite.texture = gate_texture

func _on_Level_all_food_collected():
	switch_to_gate()
