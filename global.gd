extends Node

var saved_menu = null
var saved_stage = null
var saved_difficulty = null
var move_to_next_stage = false

func save_to_global(menutype, stage, diff):
	saved_menu = menutype
	saved_stage = stage
	saved_difficulty = diff